# Gradle Android dx plugin

Plugin allows you to pack dependencies (.jar files) into android 'assets' folder (.dex files), which can be loaded later using DexClassLoader class. In partucular it helps solve 'LinearAlloc exceeded capacity' issue on old android devices.

  * https://stackoverflow.com/questions/8413898

Use 'AssetsDexLoader' to load dex:

  * https://gitlab.com/axet/android-library/blob/master/src/main/java/com/github/axet/androidlibrary/app/AssetsDexLoader.java

# Example

Plugin gradle script 100% equvalent:

```gradle
configurations {
    assets
}

clean.doFirst {
    if (!project.configurations.assets.empty) {
        delete fileTree('src/main/assets') {
            include '**/*.dex'
        }
    }
}

task processAssetsDexs() {
    doLast {
        configurations.assets.each { File file ->
            exec {
                commandLine "${android.getSdkDirectory().getAbsolutePath()}/build-tools/${android.buildToolsVersion}/dx",
                        '--dex',
                        "--output=${project.projectDir}/src/main/assets/${file.name.take(file.name.lastIndexOf('.'))}.dex",
                        "${file.getAbsolutePath()}"
            }
        }
    }
}

project.afterEvaluate {
    preBuild.dependsOn processAssetsDexs
}
```

Script above using 'dx' script from android build-tools, 'plugin' using 'com.google.android.tools:dx'.

# Adding .dex

Example how to add dependecies as .dex assets files:

```gradle
buildscript {
    dependencies {
        classpath 'com.github.axet:gradle-android-dx:0.0.2'
    }
}

apply plugin: 'com.github.axet.dxplugin'

dependencies {
    assets('com.madgag.spongycastle:bctls-jdk15on:1.58.0.0') { exclude module: 'junit' }
}
```

# Manual install

    # gradle publishToMavenLocal

# Additional Tasks

You can create manual tasks to extract .dex into specified directory:

```gradle
configurations {
    dexs
}

// AssetsDex task.
//
// Create corresponds .dex files for each input dependecies.
//
task ExtractDexs(type: com.github.axet.dxplugin.AssetsDex) {
    from configurations.dexs
    into 'src/main/assets'
}

// MergeDex task.
//
// Convert a set of classfiles [<file>.class | <file>.{zip,jar,apk} | <directory>]
// into a dex file, optionally embedded in a jar/zip. Output name must end with
// one of: .dex .jar .zip .apk or be a directory.
//
task PackDexs(type: com.github.axet.dxplugin.MergeDex) {
    from "lib1.jar"
    into 'src/main/assets/lib1.dex'
}

project.afterEvaluate {
    preBuild.dependsOn ExtractDexs
}

dependencies {
    dexs('com.madgag.spongycastle:bctls-jdk15on:1.58.0.0') { exclude module: 'junit' }
}
```