package com.github.axet.dxplugin;

import org.gradle.api.DefaultTask;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.tasks.util.PatternSet;

import java.io.File;

public class DxPlugin implements Plugin<Project> {

    public void apply(Project project) {
        project.getTasks().getByName("clean").doFirst((task) -> {
            if (!project.getConfigurations().getByName("assets").isEmpty()) {
                project.delete(project.fileTree("src/main/assets").matching(new PatternSet().include("**/*.dex")));
            }
        });

        project.getConfigurations().create("assets");

        Task processAssetsDexs = project.getTasks().create("processAssetsDexs", DefaultTask.class, (task) -> {
            task.doLast((last) -> {
                project.getConfigurations().getByName("assets").forEach((file) -> {
                    AssetsDex.dex(file, new File(project.getProjectDir(), "/src/main/assets/"));
                });
            });
        });

        project.afterEvaluate((task) -> {
            project.getTasks().getByName("preBuild").dependsOn(processAssetsDexs);
        });
    }

}
