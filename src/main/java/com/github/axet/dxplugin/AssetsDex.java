package com.github.axet.dxplugin;

import org.gradle.api.tasks.Copy;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class AssetsDex extends Copy {
    public static void dex(File file, File into) {
        try {
            String name = file.getName();
            name = name.substring(0, name.lastIndexOf('.'));
            File tmp = null;
            try {
                ZipFile zip = new ZipFile(file);
                for (Enumeration ee = zip.entries(); ee.hasMoreElements(); ) {
                    ZipEntry entry = (ZipEntry) ee.nextElement();
                    if (entry.getName().equals("classes.jar")) {
                        InputStream is = zip.getInputStream(entry);
                        tmp = File.createTempFile(name, ".jar");
                        FileOutputStream os = new FileOutputStream(tmp);
                        byte[] buf = new byte[4096];
                        int len;
                        while ((len = is.read(buf)) > 0) {
                            os.write(buf, 0, len);
                        }
                        os.close();
                        is.close();
                        file = tmp;
                    }
                }
            } catch (Exception ignore) {
            }
            String path = file.getAbsolutePath();
            com.android.dx.command.Main.main(new String[]{
                    "--dex",
                    "--output=" + new File(into, name + ".dex").getAbsolutePath(),
                    path
            });
            if (tmp != null)
                tmp.delete();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    @TaskAction
    protected void copy() {
        final File into = getDestinationDir();
        if (!into.exists() && !into.mkdirs())
            throw new RuntimeException("unable to create dir " + into);
        getSource().forEach((file) -> {
            dex(file, into);
        });
    }
}
