package com.github.axet.dxplugin;

import org.gradle.api.tasks.Copy;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.util.ArrayList;

public class MergeDex extends Copy {
    public static void dex(File[] ff, File into) {
        try {
            ArrayList<String> list = new ArrayList();
            list.add("--dex");
            list.add("--output=" + into.getAbsolutePath());
            for (File f : ff) {
                list.add(f.getAbsolutePath());
            }
            com.android.dx.command.Main.main(list.toArray(new String[]{}));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    @TaskAction
    protected void copy() {
        dex(getSource().getFiles().toArray(new File[]{}), getDestinationDir());
    }
}
